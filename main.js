const Client  = require('mongodb').MongoClient,
      program = require('commander')

program
  .option('-p, --port <port>', 'The port to register at')
  .option('-i, --id <id>', '5 first chars of the user id')
  .parse(process.argv)

let port = program.port
let id   = program.id

if (!port || !id) {
  console.error('Missing arguments')
  process.exit(1)
}

const url = 'mongodb://localhost:' + port + '/avery-' + id


Client.connect(url, function (err, db) {
  if (err) {
    console.error('Connection error: ', err)
    process.exit(1)
  }

  let blockCollection = db.collection('blocks')
  blockCollection.find({'_id': {$ne: 'latest'}}).sort({'content.height': 1}).toArray(function (err, documents) {
    if (err) {
      console.error('Find documents error:', err)
      process.exit(1)
    }

    console.log('Found', documents.length, 'documents')
    if (documents.length === 0) process.exit(1)

    let lastId, lastTimestamp, lastHeight
    let idErrorCount = 0, heightErrorCount = 0
    let avgNonce     = 0, minNonce, maxNonce
    let avgTime      = 0, minTime, maxTime

    for (let i = 0; i < documents.length; i++) {
      const block     = documents[i]
      const nonce     = block.content.nonce
      const timestamp = Number(block.content.timeStamp)
      const height    = block.content.height

      if (block.content.hashPrevBlock === "null" || !lastId) { // Discard genesis block
        lastId     = block.id
        lastHeight = height
        continue
      }

      avgNonce += nonce

      if (!minNonce) minNonce = nonce
      else minNonce = Math.min(minNonce, nonce)

      if (!maxNonce) maxNonce = nonce
      else maxNonce = Math.max(maxNonce, nonce)

      if (lastTimestamp) {
        const timeSinceLastBlock = timestamp - lastTimestamp
        avgTime += timeSinceLastBlock

        if (!minTime) minTime = timeSinceLastBlock
        else minTime = Math.min(minTime, timeSinceLastBlock)

        if (!maxTime) maxTime = timeSinceLastBlock
        else maxTime = Math.max(maxTime, timeSinceLastBlock)
      }

      if (block.content.hashPrevBlock !== lastId) {
        idErrorCount++
        console.error('// -- HASH ERROR -- //\nLast id: ' + lastId + ' doesn\'t match hashPrevBlock in block:\n', JSON.stringify(block) + '\n')
      }

      if (height !== lastHeight + 1) {
        heightErrorCount++
        console.error('// -- HEIGHT ERROR -- //\nLast height: ' + lastHeight + ' doesn\'t match height in block:\n', JSON.stringify(block) + '\n')
      }

      lastId        = block.id
      lastTimestamp = timestamp
      lastHeight    = height
    }

    avgNonce = avgNonce / documents.length
    avgTime  = avgTime / documents.length

    if (idErrorCount === 0 && heightErrorCount === 0) {
      console.log('Validation completed with success.')
    } else {
      let idErrString = 'id errors', heightErrString = 'height errors'
      if (idErrorCount === 1) idErrString = 'id error'
      if (heightErrorCount === 1) heightErrString = 'height error'
      console.log('Validation completed with ' + idErrorCount + ' ' + idErrString + ' and ' + heightErrorCount + ' ' + heightErrString + '.')
    }

    console.log('Avg nonce:', Number(avgNonce).toFixed(0))
    console.log('Min nonce:', minNonce)
    console.log('Max nonce:', maxNonce)

    console.log('Avg time:', Number(avgTime / 1000).toFixed(2), 'sec')
    console.log('Min time:', Number(minTime / 1000).toFixed(2), 'sec')
    console.log('Max time:', Number(maxTime / 1000).toFixed(2), 'sec')
    process.exit(1)
  })
})